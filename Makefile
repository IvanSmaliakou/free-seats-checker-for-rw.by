IMAGE_URL=${GITLAB_REGISTRY}/${GITLAB_USER}/free-seats-checker-for-rw.by

server_access: ssh_perms
	echo ${SSH_PRIVATE_KEY} >> ~/.ssh/free_seats_server_key

ssh_perms: .ssh
	chmod 700 ~/.ssh/free_seats_server_key

.ssh:
	mkdir -p ~/.ssh

docker_build:
	docker build -f Dockerfile -t free-seats-checker --build-arg REDIS_HOST=${REDIS_HOST} --build-arg REDIS_PORT=${REDIS_PORT} --build-arg BOT_TOKEN=${BOT_TOKEN} .

docker_tag: docker_build
	docker tag free-seats-checker:latest ${IMAGE_URL}

docker_login:
	docker login ${GITLAB_REGISTRY} -u ${GITLAB_USER} -p ${GITLAB_TOKEN}

docker_push: docker_tag docker_login
	docker push ${IMAGE_URL}

check_docker_host:
	if [ -z "$(DOCKER_HOST)" ]; then exit -1; fi

stop_previous: check_docker_host
	docker-compose down

deploy_server: docker_build stop_previous check_docker_host
	docker-compose -f docker-compose.yml up -d
