const {Telegraf, Markup} = require('telegraf');
const Calendar = require('telegraf-calendar-telegram');
const fetch = require('node-fetch');
const jquery = require("jquery");
const {JSDOM} = require("jsdom");
const moment = require('moment');
const redis = require('redis');

const botToken = process.env.BOT_TOKEN;
const bot = new Telegraf(botToken);
let redisClient;
const RW_HOST = process.env.RW_HOST || 'https://pass.rw.by';

const calendar = new Calendar(bot, {
    startWeekDay: 1,
    weekDayNames: ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"],
    monthNames: [
        "Янв", "Фев", "Март", "Апрель", "Май", "Июнь",
        "Июль", "Авг", "Сен", "Окт", "Нояб", "Дек"
    ]
});

const singleSeatsNumbers = [1, 4, 5, 10, 13, 16, 17, 22, 25];

let levels = {
    departurePoint: 0,
    arrivalPoint: 1,
    datePicking: 2,
    trainPicking: 3,
    freeSeatsChecking: 4,
};
levels = Object.freeze(levels);

class BotError extends Error {
    constructor(msg, ctx) {
        super(msg);
        this.ctx = ctx;
        this.name = 'Ошибка';
    }
}

class ValidationError extends BotError {
    constructor(msg, ctx) {
        super(msg, ctx);
        this.name = 'Некорректные данные';
    }
}

const errHandler = function (e) {
    e.ctx.reply(e.toString());
}

function freeSingleSeatsCount(seats) {
    let singleSeatsCount = 0;

    seats.forEach(seatNumber => {
        if (singleSeatsNumbers.includes(parseInt(seatNumber))) {
            singleSeatsCount += 1;
        }
    });
    return singleSeatsCount;
}

const startBot = async function (handleError) {
    const cyrillicPattern = new RegExp('^[а-яА-Я]+$');
    bot.start((ctx) => {
        redisClient.hmset(ctx.message.from.id, {currentLevel: levels.departurePoint});
        ctx.reply('Станция отправления:', Markup.removeKeyboard(true));
    });
    bot.on('text', async (ctx) => {
            redisClient.hgetall(ctx.message.from.id, async (err, userData) => {
                if (err) {
                    console.error(err);
                }
                switch (Number.parseInt(userData.currentLevel)) {
                    case levels.departurePoint:
                        if (!cyrillicPattern.test(ctx.message.text)) {
                            return handleError(new ValidationError('неверное имя точки отправления.', ctx));
                        }
                        redisClient.hmset(ctx.message.from.id, {
                            departurePoint: ctx.message.text,
                            currentLevel: levels.arrivalPoint
                        });
                        ctx.reply('Станция назначения:');
                        return;
                    case levels.arrivalPoint:
                        let now = new Date();
                        let nowPlusMonth = new Date();
                        nowPlusMonth.setMonth(nowPlusMonth.getMonth() + 1);

                        if (!cyrillicPattern.test(ctx.message.text)) {
                            return handleError(new ValidationError('неверное имя точки назначения.', ctx));
                        }
                        redisClient.hmset(ctx.message.from.id, {
                            arrivalPoint: ctx.message.text,
                            currentLevel: levels.datePicking
                        });
                        ctx.reply('Дата отправления:', calendar.setMinDate(now).setMaxDate(nowPlusMonth).getCalendar());
                        return;

                    case levels.trainPicking:
                        let trains = JSON.parse(userData.trains);
                        let selectedTrain = {};
                        for (const train of trains) {
                            if (ctx.message.text.startsWith(`${train.number} - `)) {
                                selectedTrain = train;
                                break;
                            }
                        }

                        let getEmptySeats = async () => {
                            let emptySeats = new Map();
                            for (let i = 0; i < 9; i++) {
                                let url = `${selectedTrain.ticketsOrderUrl}&car_type=${i}`;
                                await fetchTrainInfo(url, emptySeats);
                            }
                            return emptySeats;
                        };

                        let emptySeats = await getEmptySeats();
                        // if every car_type returns an error (usually happens when there are no seats available on the train).
                        let checkUrls = [...Array(10).keys()].reduce((acc, i) => {
                            acc.push(`${selectedTrain.ticketsOrderUrl}&car_type=${i}`);
                            return acc;
                        }, []);

                        let includes1cSingle = false;
                        let msg = Object.entries(emptySeats).reduce((acc, [type, tariffInfo]) => {
                            if (type.includes('1С')) {
                                let singleSeatsCount = freeSingleSeatsCount(tariffInfo.emptySeats);
                                if (singleSeatsCount) {
                                    includes1cSingle = true;
                                    return acc + `${type} - ${tariffInfo.emptySeats.length}; из них одиночных - ${singleSeatsCount}\n`;
                                } else {
                                    checkUrls = tariffInfo.urls;
                                }
                            }
                            return acc + `${type} - ${tariffInfo.emptySeats.length};\n`;
                        }, '');
                        if (!msg) {
                            msg = 'Нет доступных мест на данный поезд'
                        }
                        if (selectedTrain.type_interregional_business && !includes1cSingle) {
                            redisClient.hmset(ctx.message.from.id, {
                                currentLevel: levels.freeSeatsChecking,
                                checkUrls: JSON.stringify(checkUrls),
                            });
                            ctx.reply(msg, Markup.keyboard([[Markup.button.callback('Запустить поиск одиночных мест в 1С')]]));
                        } else {
                            ctx.reply(msg, Markup.removeKeyboard(true));
                        }
                        return;

                    case levels.freeSeatsChecking:
                        let promiseWithFreeSeatsCount = startFreeSeatsChecking(JSON.parse(userData.checkUrls));
                        ctx.reply('Начинается поиск доступных одиночных мест в 1С', Markup.removeKeyboard(true));
                        promiseWithFreeSeatsCount.then((freeSeatsCount) => {
                            ctx.reply(`Доступно ${freeSeatsCount} одиночных мест в 1С`);
                        })
                }
            });
        }
    );
    calendar.setDateListener(async (ctx, date) => {
        redisClient.hgetall(ctx.update.callback_query.from.id, async (err, routeInfo) => {
            let url = buildQueryUrl(routeInfo.departurePoint, routeInfo.arrivalPoint, date);
            let trains = await startRWServicePolling(url);
            redisClient.hmset(ctx.update.callback_query.from.id, {
                trains: JSON.stringify(trains),
                currentLevel: levels.trainPicking
            });
            let buttons = [];
            trains.forEach(train => {
                const button = Markup.button.callback(`${train.number} - ${train.route}; Отпр. ${train.departure}; Приб. ${train.arrival}; В пути - ${train.duration}`);
                buttons.push([button]);
            });
            ctx.reply('Выберете поезд: ', Markup.keyboard(buttons));
        });
    });
}

async function startRWServicePolling(url) {
    let resp = await fetch(url);
    let respHtml = await resp.text();
    const {window} = (new JSDOM(respHtml));
    const $ = jquery(window);
    const getTrains = async () => {
        let trains = [];
        for (let i = 0; i < $('.sch-table__row-wrap:not(.disabled)').length; i++) {
            const number = $(`.sch-table__row-wrap:not(.disabled):nth-child(${i + 1}) .train-number`).html().split('\n').join('');
            const departure = $(`.sch-table__row-wrap:not(.disabled):nth-child(${i + 1}) .train-from-time:not(.disabled)`).html().split(' ').join('').split('\n').join('');
            const dataTrainInfo = JSON.parse($(`.sch-table__body div[data-train-info]:not(.disabled):nth-child(${i + 1})`).attr('data-train-info').split(' ').join('').split('\n').join(''));
            const date = dataTrainInfo.date;
            const fromCode = dataTrainInfo.from;
            const toCode = dataTrainInfo.to;
            const departureTs = moment(`${date} ${departure}`, 'YYYY-MM-DD HH:mm').unix();
            const ticketsOrderUrl = `${RW_HOST}/ru/ajax/route/car_places/?from=${fromCode}&to=${toCode}&date=${date}&train_number=${number}&from_time=${departureTs}`;

            let train = {
                route: $(`.sch-table__row-wrap:not(.disabled):nth-child(${i + 1}) .train-route`).html().replace('&nbsp;', ' ').split(' ').join('').split('\n').join(''),
                arrival: $(`.sch-table__row-wrap:not(.disabled):nth-child(${i + 1}) .train-to-time`).html().split(' ').join('').split('\n').join(''),
                duration: $(`.sch-table__row-wrap:not(.disabled):nth-child(${i + 1}) .train-duration-time`).html().split('\n').join(''),
                type_interregional_business: $(`.sch-table__row-wrap:not(.disabled) .svg-interregional_business:not(.disabled)`).length > 0,
                ticketsOrderUrl,
                number,
                departure,
            };
            trains.push(train);
        }
        return trains;
    };
    return getTrains();
}

async function startFreeSeatsChecking(urls) {
    let intervalSecs = 30 + Math.floor(Math.random() * 30);
    return new Promise(res => {
        setInterval(async () => {
            let resMap = new Map();
            for (const url of urls) {
                await fetchTrainInfo(url, resMap);
            }
            for (const [key, val] of Object.entries(resMap)) {
                if (key.includes('1С')) {
                    let numOfFreeSeats = freeSingleSeatsCount(val.emptySeats);
                    if (numOfFreeSeats) {
                        return res(numOfFreeSeats);
                    }
                }
            }
        }, intervalSecs * 1000);
    })
}

function buildQueryUrl(from, to, dateString) {
    const query = `/ru/route/?from=${from}&to=${to}&date=${dateString}`;
    return encodeURI(RW_HOST + query);
}

async function fetchTrainInfo(url, emptySeats) {
    let resp = await fetch(encodeURI(url));
    let body = await resp.json();
    if (body.e || !body.tariffs.length) {
        return;
    }
    body.tariffs.forEach(tariff => {
        tariff.cars.forEach(car => {
            if (!emptySeats[tariff.type]) {
                emptySeats[tariff.type] = {
                    emptySeats: [],
                    urls: [],
                };
            }
            emptySeats[tariff.type] = {
                emptySeats: emptySeats[tariff.type].emptySeats.concat(car.emptyPlaces),
                urls: emptySeats[tariff.type].urls.concat(url),
                trainType: body.trainType,
            };
        });
    });
}

async function connectToRedis() {
    const host = process.env.REDIS_HOST || 'localhost';
    const port = process.env.REDIS_PORT || 6379;
    redisClient = redis.createClient({host: host, port: port, connect_timeout: 10000});
    return new Promise((resolve, reject) => {
        redisClient.on('error', error => reject('Error Connecting to the Redis Cluster', error));
        redisClient.on('connect', () => {
            resolve();
        });
    });
}

connectToRedis().then(() => {
    startBot(errHandler).then(() => console.log('Bot started')).catch(console.error);
    bot.launch().catch(err => console.error(err));
}).catch(console.error)
